import { createContext } from "react";

export const StateContext = createContext();
export const StateProvider = StateContext.Provider;
