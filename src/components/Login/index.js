import React, { useState } from "react";
import styled from "styled-components";
import { string, func } from "prop-types";

const LoginWrapper = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;

  > * {
    margin: 1rem 0;
  }
`;

const Login = ({ nickname, setNickname, setStartGame }) => {
  const [errorMsg, setErrorMsg] = useState("");

  const tryStartGame = () => {
    setErrorMsg("");

    if (nickname.length) {
      setStartGame(true);
    } else {
      setErrorMsg("Please enter Your nickname...");
    }
  };

  return (
    <LoginWrapper>
      <h2>CooKill.IO</h2>
      <label>Enter nickname</label>
      <input
        type="text"
        value={nickname}
        onChange={event => setNickname(event.target.value)}
      />
      <button onClick={tryStartGame}>Join Game!</button>
      <p>{errorMsg}</p>
    </LoginWrapper>
  );
};

Login.propTypes = {
  nickname: string.isRequired,
  setNickname: func.isRequired
};

export default Login;
