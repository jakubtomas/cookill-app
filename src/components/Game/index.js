import React, { useReducer, useEffect, useState, useRef } from "react";
import io from "socket.io-client";
import { string } from "prop-types";

import { StateProvider } from "../../constants/StateProvider";
import {
  gameReducer,
  initialState,
  JOINED_GAME,
  UPDATE_ENEMIES
} from "./reducer";
import { countDimensions } from "./actions";

import Map from "../Map";

const socket = io("http://localhost:5000");

const Game = ({ nickname }) => {
  const [state, dispatch] = useReducer(gameReducer, initialState);
  const [gameIsReady, setGameIsReady] = useState(false);
  const dimensionRef = useRef(state.dimension);

  useEffect(() => {
    connectToServer();
    window.addEventListener("resize", () =>
      dispatch(countDimensions(dimensionRef.current))
    );

    return () => dispatch(countDimensions());
  }, []);

  useEffect(() => {
    dimensionRef.current = state.dimension;
  }, [state.tileWidth, state.tileHeight]);

  useEffect(() => prepareDataForServer(), [
    state.playerPosition,
    state.bullets
  ]);

  const listenToNewData = id =>
    socket.on("recieveGameData", data =>
      dispatch({
        type: UPDATE_ENEMIES,
        enemies: data.filter(enemy => enemy.id !== id)
      })
    );

  const saveJoinGameData = data => {
    dispatch({
      type: JOINED_GAME,
      player: data.player,
      room: data.room,
      spawnPlace: data.room.spawnPlace,
      dimension: data.room.mapDimension,
      enemies: data.room.players.filter(enemy => enemy.id !== data.player.id)
    });
    dispatch(countDimensions(data.room.mapDimension));
    setGameIsReady(true);
    listenToNewData(data.player.id);
  };

  const connectToServer = () => {
    socket.emit("joinGame", { nickname });
    socket.on("joinedGame", data => saveJoinGameData(data));
  };

  const prepareDataForServer = () => {
    const dataForServer = {
      [state.id]: {
        playerPosition: state.playerPosition,
        bullets: state.bullets,
        room: state.room
      }
    };

    socket.emit("sendPlayerData", dataForServer);
  };

  return (
    <StateProvider value={{ state, dispatch }}>
      {gameIsReady && <Map />}
    </StateProvider>
  );
};

Game.propTypes = {
  nickname: string.isRequired
};

export default Game;
