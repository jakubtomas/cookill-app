import { CHANGE_TILE_SIZE } from "./reducer";

export const countDimensions = dimension => {
  const tileWidth = document.documentElement.clientWidth / dimension[0];
  const tileHeight = document.documentElement.clientHeight / dimension[1];

  return {
    type: CHANGE_TILE_SIZE,
    width: tileWidth,
    height: tileHeight
  };
};
