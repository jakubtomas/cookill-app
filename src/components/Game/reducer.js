export const CHANGE_PLAYER_POSITION = "CHANGE_PLAYER_POSITION";
export const CHANGE_BULLET_POSITION = "CHANGE_BULLET_POSITION";
export const UPDATE_BULLETS = "UPDATE_BULLETS";
export const JOINED_GAME = "JOINED_GAME";
export const UPDATE_ENEMIES = "UPDATE_ENEMIES";
export const CHANGE_TILE_SIZE = "CHANGE_TILE_SIZE";

export const initialState = {
  room: null,
  id: null,
  nickname: null,
  playerPosition: null,
  bullets: null,
  map: null,
  spawnPlace: null,
  enemies: null,
  dimension: null,
  tileWidth: null,
  tileHeight: null
};

export const gameReducer = (state, action) => {
  switch (action.type) {
    case CHANGE_PLAYER_POSITION:
      return {
        ...state,
        playerPosition: action.position
      };

    case UPDATE_BULLETS:
      return {
        ...state,
        bullets: action.bullets
      };

    case CHANGE_BULLET_POSITION:
      const { id, position } = action;

      let bullets = [...state.bullets];
      let bullet = { ...bullets.find(bullet => bullet[2] === id) };

      bullets.forEach((bull, index) => {
        if (bull[2] === id) {
          bullet[0] = position[0];
          bullet[1] = position[1];

          const newBulletArray = Object.values(bullet);

          bullets[index] = newBulletArray;
        }
      });

      return {
        ...state,
        bullets: bullets
      };

    case JOINED_GAME:
      const { player, room } = action;

      return {
        ...state,
        id: player.id,
        room: player.room,
        nickname: player.nickname,
        playerPosition: player.playerPosition,
        bullets: player.bullets,
        map: room.map,
        enemies: action.enemies,
        spawnPlace: action.spawnPlace,
        dimension: action.dimension
      };

    case UPDATE_ENEMIES:
      return {
        ...state,
        enemies: action.enemies
      };

    case CHANGE_TILE_SIZE:
      return {
        ...state,
        tileWidth: action.width,
        tileHeight: action.height
      };

    default:
      return state;
  }
};
