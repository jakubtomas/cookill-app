import React from "react";
import styled from "styled-components";

const EnemySprite = styled.div`
  transition: all 0.3s linear;
  width: 40px;
  height: 40px;
  background: red;
  position: absolute;
  left: ${({ position }) => `${position[0] * 40}px`};
  top: ${({ position }) => `${position[1] * 40}px`};
  box-sizing: border-box;
  /* border-top: ${({ direction }) => direction === "UP" && "4px solid red"};
  border-right: ${({ direction }) => direction === "RIGHT" && "4px solid red"};
  border-bottom: ${({ direction }) => direction === "DOWN" && "4px solid red"};
  border-left: ${({ direction }) => direction === "LEFT" && "4px solid red"}; */
`;

const Enemy = ({ position }) => {
  return <EnemySprite position={position} />;
};

export default Enemy;
