import React, { Fragment, useContext } from "react";

import { StateContext } from "../../constants/StateProvider";

import Enemy from "./Enemy";

const Enemies = () => {
  const { state } = useContext(StateContext);
  const { enemies } = state;

  return (
    <Fragment>
      {enemies.map(({ id, playerPosition }) => (
        <Enemy position={playerPosition} key={id} />
      ))}
    </Fragment>
  );
};

export default Enemies;
