import React, { useState, useEffect, useRef, useContext } from "react";
import styled from "styled-components";

import { DIRECTIONS } from "../../constants";
import { StateContext } from "../../constants/StateProvider";
import { CHANGE_PLAYER_POSITION, UPDATE_BULLETS } from "../Game/reducer";

import Bullet from "../Bullet";

const PlayerSprite = styled.div`
  transition: all 0.3s linear;
  width: ${({ width }) => `${width}px`};
  height: ${({ height }) => `${height}px`};
  background: blue;
  position: absolute;
  left: ${({ position, width }) => `${position[0] * width}px`};
  top: ${({ position, height }) => `${position[1] * height}px`};
  box-sizing: border-box;
  border-top: ${({ direction }) => direction === "UP" && "4px solid red"};
  border-right: ${({ direction }) => direction === "RIGHT" && "4px solid red"};
  border-bottom: ${({ direction }) => direction === "DOWN" && "4px solid red"};
  border-left: ${({ direction }) => direction === "LEFT" && "4px solid red"};
  z-index: 1;
`;

const Player = () => {
  const { state, dispatch } = useContext(StateContext);
  const { map, spawnPlace, tileWidth, tileHeight, dimension } = state;

  const [positionInMatrix, setPosInMatrix] = useState([
    spawnPlace[0],
    spawnPlace[1]
  ]);
  const [direction, setDirection] = useState(DIRECTIONS.RIGHT);

  const [bullets, setBullets] = useState([]);

  const posInMtrxRef = useRef(positionInMatrix);
  const canMove = useRef(true);
  const keyMapper = useRef({});
  const shooting = useRef(false);

  useEffect(() => {
    document.addEventListener("keydown", event => recognizeEvent(event));
    document.addEventListener("keyup", event => recognizeEvent(event));

    return () => {
      document.removeEventListener("keydown", recognizeEvent);
      document.removeEventListener("keyup", recognizeEvent);
    };
  }, []);

  useEffect(() => {
    posInMtrxRef.current = positionInMatrix;

    dispatch({
      type: CHANGE_PLAYER_POSITION,
      position: positionInMatrix
    });
  }, [positionInMatrix]);

  useEffect(() => {
    dispatch({ type: UPDATE_BULLETS, bullets });
  }, [bullets]);

  const validKeyPressed = key => {
    if (
      key === "KeyW" ||
      key === "KeyD" ||
      key === "KeyS" ||
      key === "KeyA" ||
      key === "Space"
    )
      return true;
  };

  const isMovementKeyPressed = () => {
    const { KeyW, KeyD, KeyS, KeyA } = keyMapper.current;

    if (KeyW || KeyD || KeyS || KeyA) return true;
  };

  const isOneKeyPressed = () => {
    const { KeyW, KeyD, KeyS, KeyA } = keyMapper.current;

    if (KeyW && !KeyD && !KeyS && !KeyA) return true;
    if (!KeyW && KeyD && !KeyS && !KeyA) return true;
    if (!KeyW && !KeyD && KeyS && !KeyA) return true;
    if (!KeyW && !KeyD && !KeyS && KeyA) return true;
  };

  const isOnBorder = dir => {
    const { current } = posInMtrxRef;

    if (dir === DIRECTIONS.UP) return current[1] - 1 > -1;
    if (dir === DIRECTIONS.DOWN) return current[1] + 1 < dimension[1];
    if (dir === DIRECTIONS.RIGHT) return current[0] + 1 < dimension[0];
    if (dir === DIRECTIONS.LEFT) return current[0] - 1 > -1;
  };

  const detectCollisionAndMove = () => {
    const { current } = posInMtrxRef;

    if (isOneKeyPressed()) {
      if (keyMapper.current.KeyW) {
        setDirection(DIRECTIONS.UP);
        const nextPlace = `${current[0]}x${current[1] - 1}`;

        if (!map[nextPlace] && isOnBorder(DIRECTIONS.UP)) {
          setPosInMatrix(prevState => [prevState[0], prevState[1] - 1]);
        }
      }

      if (keyMapper.current.KeyS) {
        setDirection(DIRECTIONS.DOWN);
        const nextPlace = `${current[0]}x${current[1] + 1}`;

        if (!map[nextPlace] && isOnBorder(DIRECTIONS.DOWN)) {
          setPosInMatrix(prevState => [prevState[0], prevState[1] + 1]);
        }
      }

      if (keyMapper.current.KeyD) {
        setDirection(DIRECTIONS.RIGHT);
        const nextPlace = `${current[0] + 1}x${current[1]}`;

        if (!map[nextPlace] && isOnBorder(DIRECTIONS.RIGHT)) {
          setPosInMatrix(prevState => [prevState[0] + 1, prevState[1]]);
        }
      }

      if (keyMapper.current.KeyA) {
        setDirection(DIRECTIONS.LEFT);
        const nextPlace = `${current[0] - 1}x${current[1]}`;

        if (!map[nextPlace] && isOnBorder(DIRECTIONS.LEFT)) {
          setPosInMatrix(prevState => [prevState[0] - 1, prevState[1]]);
        }
      }
    }
  };

  const performMove = () => {
    if (canMove.current) {
      canMove.current = false;
      detectCollisionAndMove();

      const timeout = setTimeout(() => {
        canMove.current = true;
        clearTimeout(timeout);
      }, 300);
    }
  };

  const bulletCollided = index =>
    setBullets(prevState => prevState.filter(bullet => bullet[2] !== index));

  const triggerShot = () => {
    const { current } = posInMtrxRef;

    if (!shooting.current) {
      shooting.current = true;
      setBullets(prevState => [
        ...prevState,
        [current[0], current[1], prevState.length + 1 * Math.random()]
      ]);

      const timeout = setTimeout(() => {
        shooting.current = false;
        clearTimeout(timeout);
      }, 300);
    }
  };

  const recognizeEvent = event => {
    const { code, type } = event;

    if (validKeyPressed(code) || code === "shootAndMove") {
      event.preventDefault();

      if (type === "keydown") {
        keyMapper.current = { ...keyMapper.current, [code]: true };

        if (code === "shootAndMove") performMove();
        if (isMovementKeyPressed()) performMove();
        if (keyMapper.current.Space) triggerShot();
      } else {
        keyMapper.current = { ...keyMapper.current, [code]: false };

        if (isMovementKeyPressed() && !keyMapper.current.Space) {
          const newEvent = new KeyboardEvent("keydown", {
            code: "shootAndMove"
          });

          setInterval(() => document.dispatchEvent(newEvent), 100);
        }
      }
    }
  };

  return (
    <div>
      <PlayerSprite
        width={tileWidth}
        height={tileHeight}
        position={positionInMatrix}
        direction={direction}
      />
      {bullets.map(bullet => (
        <Bullet
          key={bullet[2]}
          startX={bullet[0]}
          startY={bullet[1]}
          index={bullet[2]}
          dir={direction}
          bulletCollided={bulletCollided}
        />
      ))}
    </div>
  );
};

export default Player;
