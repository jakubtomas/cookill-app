import React from "react";
import styled from "styled-components";

import placeholder from "../../assets/placeholder.jpg";

import Player from "../Player";
import Enemies from "../Enemies";
import Grid from "./Grid";

const MapTile = styled.div`
  border: 1px solid black;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-image: url(${placeholder});
  background-size: 100% 100%;
`;

const Map = () => {
  return (
    <MapTile>
      <Player />
      {/* <Grid /> */}
      {/* <Enemies /> */}
    </MapTile>
  );
};

export default Map;
