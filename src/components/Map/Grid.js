import React, { useContext } from "react";
import styled from "styled-components";

import { StateContext } from "../../constants/StateProvider";

const Tile = styled.div`
  width: ${({ width }) => width}px;
  height: ${({ height }) => height}px;
  box-sizing: border-box;
  border-right: 1px solid white;
  border-bottom: 1px solid white;
  color: yellow;
  text-align: right;
`;

const GridContainer = styled.div`
  display: flex;
`;

const Row = styled.div`
  position: absolute;
  left: 10px;
  color: white;
`;

const Grid = () => {
  const { state } = useContext(StateContext);

  const { dimension, tileWidth, tileHeight } = state;

  return (
    <div>
      {[...Array(dimension[1])].map((_, index) => (
        <GridContainer key={index}>
          <Row>{index}</Row>
          {index === 0 &&
            [...Array(dimension[0])].map((_, firstRowIndex) => (
              <Tile key={firstRowIndex} width={tileWidth} height={tileHeight}>
                {firstRowIndex}
              </Tile>
            ))}
          {index > 0 &&
            [...Array(dimension[0])].map((_, columnIndex) => (
              <Tile key={columnIndex} width={tileWidth} height={tileHeight} />
            ))}
        </GridContainer>
      ))}
    </div>
  );
};

export default Grid;
