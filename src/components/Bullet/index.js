import React, { useEffect, useState, useContext } from "react";
import styled from "styled-components";
import { number, func, string } from "prop-types";

import { DIRECTIONS } from "../../constants";
import { StateContext } from "../../constants/StateProvider";
import { CHANGE_BULLET_POSITION } from "../Game/reducer";

const Container = styled.div`
  width: ${({ width }) => `${width}px`};
  height: ${({ height }) => `${height}px`};
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  left: ${({ posX, width }) => `${posX * width}px`};
  top: ${({ posY, height }) => `${posY * height}px`};
`;

const BulletSprite = styled.div`
  width: 10px;
  height: 10px;
  background: yellow;
`;

const Bullet = ({ startX, startY, dir, bulletCollided, index }) => {
  const { state, dispatch } = useContext(StateContext);
  const { map, tileWidth, tileHeight } = state;

  const [bulletPosition, setBulletPosition] = useState([startX, startY]);
  const [direction] = useState(dir);

  useEffect(() => {
    moveBullet();

    dispatch({
      type: CHANGE_BULLET_POSITION,
      id: index,
      position: bulletPosition
    });
  }, [bulletPosition]);

  const isOnBorder = dir => {
    const { dimension } = state;

    if (direction === DIRECTIONS.UP) return bulletPosition[1] - 1 > -1;
    if (direction === DIRECTIONS.DOWN)
      return bulletPosition[1] + 1 < dimension[1];
    if (direction === DIRECTIONS.RIGHT)
      return bulletPosition[0] + 1 < dimension[0];
    if (direction === DIRECTIONS.LEFT) return bulletPosition[0] - 1 > -1;
  };

  const detectCollisionAndMoveBullet = () => {
    if (direction === DIRECTIONS.UP) {
      const nextPlace = `${bulletPosition[0]}x${bulletPosition[1] - 1}`;

      if (!map[nextPlace] && isOnBorder(DIRECTIONS.UP)) {
        setBulletPosition(prevState => [prevState[0], prevState[1] - 1]);
      } else {
        bulletCollided(index);
      }
    }

    if (direction === DIRECTIONS.DOWN) {
      const nextPlace = `${bulletPosition[0]}x${bulletPosition[1] + 1}`;

      if (!map[nextPlace] && isOnBorder(DIRECTIONS.DOWN)) {
        setBulletPosition(prevState => [prevState[0], prevState[1] + 1]);
      } else {
        bulletCollided(index);
      }
    }

    if (direction === DIRECTIONS.RIGHT) {
      const nextPlace = `${bulletPosition[0] + 1}x${bulletPosition[1]}`;

      if (!map[nextPlace] && isOnBorder(DIRECTIONS.RIGHT)) {
        setBulletPosition(prevState => [prevState[0] + 1, prevState[1]]);
      } else {
        bulletCollided(index);
      }
    }

    if (direction === DIRECTIONS.LEFT) {
      const nextPlace = `${bulletPosition[0] - 1}x${bulletPosition[1]}`;

      if (!map[nextPlace] && isOnBorder(DIRECTIONS.LEFT)) {
        setBulletPosition(prevState => [prevState[0] - 1, prevState[1]]);
      } else {
        bulletCollided(index);
      }
    }
  };

  const moveBullet = () => {
    const shot = setTimeout(() => {
      detectCollisionAndMoveBullet();
      clearTimeout(shot);
    }, 50);
  };

  return (
    <Container
      width={tileWidth}
      height={tileHeight}
      posX={bulletPosition[0]}
      posY={bulletPosition[1]}
    >
      {console.log()}
      <BulletSprite />
    </Container>
  );
};

Bullet.propTypes = {
  startX: number.isRequired,
  startY: number.isRequired,
  dir: string.isRequired,
  bulletCollided: func.isRequired
};

export default Bullet;
