import React, { useState } from "react";
import styled from "styled-components";

import Login from "./components/Login/";
import Game from "./components/Game/";

const GameWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const App = () => {
  const [nickname, setNickname] = useState("kuba");
  const [startGame, setStartGame] = useState(true);

  return (
    <GameWrapper>
      {!startGame && (
        <Login
          nickname={nickname}
          setNickname={name => setNickname(name)}
          setStartGame={setStartGame}
        />
      )}
      {startGame && <Game nickname={nickname} />}
    </GameWrapper>
  );
};

export default App;
